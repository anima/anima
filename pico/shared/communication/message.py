import json

class Message:
    # Keys to build the message dictionary
    M_DICT_KEY_TMSTP = "t"
    M_DICT_KEY_CTG = "c"
    M_DICT_KEY_DATA = "d"
    M_DICT_KEY_EMITTER = "e"
    
    # Message categories (no enum in python)
    M_CTG_SENSOR = "S"    # Emitted by pico only
    M_CTG_RAW = "R"       # Pico will most likely do not know how to interpret this
    M_CTG_COMMAND = "C"   # Pico receive them only
    M_CTG_ERROR = "E"     # When you want to notify something wrong occured
    M_CTG_UNKNOWN = "U"   # When message was not following the protocol

    timestamp: int
    category: str
    data: str
    emitter: str

    def __init__(self, timestamp: int, category: str, data:str , emitter: str):
        self.timestamp = timestamp
        self.category = category
        self.data = data
        self.emitter = emitter

    def to_json(self):
        mDict = {}

        mDict[Message.M_DICT_KEY_TMSTP] = str(self.timestamp)
        mDict[Message.M_DICT_KEY_CTG] = self.category
        mDict[Message.M_DICT_KEY_DATA] = self.data
        mDict[Message.M_DICT_KEY_EMITTER] = self.emitter

        return json.dumps(mDict)
    
    @staticmethod
    def from_json(raw_message: str):
        """Raw message which should be a json like object.

        `Message.M_DICT_KEY_TMSTP` and `Message.M_DICT_KEY_EMITTER` are mandatory in the `raw_message`. \
        If unset, a KeyError will thrown.
        
        Other values will be replaced with default values.

        Args:
            raw_message (str): JSON representation of a `Message`. See method description for details. 

        Returns:
            Message: An instanciated message.
        """
        try:
            mDict = json.loads(raw_message)

            return Message(mDict[Message.M_DICT_KEY_TMSTP], 
                            mDict.get(Message.M_DICT_KEY_CTG, Message.M_CTG_UNKNOWN), 
                            mDict[Message.M_DICT_KEY_DATA],
                            mDict.get(Message.M_DICT_KEY_EMITTER, "unknown")
            )
        except json.decoder.JSONDecodeError:
            return Message(-1,
                            Message.M_CTG_UNKNOWN,
                            raw_message,
                            "unknown")



