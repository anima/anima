from shared.communication.message import Message
from shared.communication import usb_data_manager
from time import time

class MessageManager:
    usb_port: any
    emitter: str

    def __init__(self, usb_port, emitter: str = "undefined"):
        self.usb_port = usb_port
        self.emitter = emitter

    def send_sensor_msg(self, sensor_name: str, sensor_value: str, value_type: str):
        m = Message(time(), 
                    Message.M_CTG_SENSOR, 
                    '{"sN":"'+sensor_name+'","sV":"'+sensor_value+'","vT":"'+value_type+'"}',
                    self.emitter)
        self.send_msg(m)

    def send_raw_msg(self, text: str):
        m = Message(time(), 
                    Message.M_CTG_RAW,
                    text,
                    self.emitter)
        self.send_msg(m)

    def send_cmd_msg(self, text: str):
        m = Message(time(), 
                    Message.M_CTG_COMMAND,
                    text,
                    self.emitter)
        self.send_msg(m)

    def send_msg(self, message: Message):
        usb_data_manager.write_data(data=message.to_json(), 
                                    usb_port=self.usb_port)

    def read_msg(self) -> Message:
        raw_msg = usb_data_manager.readline(usb_port=self.usb_port)
        return Message.from_json(raw_msg)