ENCODING = "utf-8"
ENDOFLINE_CHAR = "\n"

def write_data(data : str, usb_port):
    """Write any data to an USB port adding needed bytes.

    Args:
        data (str): String to write in USB.
        usb_port (Any): Can be either a Serial or a sys.stdin.buffer
    """
    usb_port.write((data + ENDOFLINE_CHAR).encode(ENCODING))

def readline(usb_port) -> str:
    received_msg = ""
    char = ""

    try:
        while char != ENDOFLINE_CHAR:
            char = usb_port.read(1).decode(ENCODING)
            received_msg += char
    except Exception as e:
        return str(e)

    return received_msg