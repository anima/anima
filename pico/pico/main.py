from machine import Pin, ADC, PWM, UART, Timer
import time
import sys
from thermometer.temperature import get_thermometer_values
from music.player import MusicPlayer
from music.discotheque import default_song
from shared.communication.message_manager import MessageManager
from shared.communication.message import Message

message_manager = MessageManager(sys.stdin.buffer, "pico")

# SETUP ELEMENTS
blue_led = PWM(Pin(15))
red_led = PWM(Pin(14))
thermometer = ADC(26)
luminosity = ADC(27)

buzzer_active_as_passive = PWM(Pin(13))
buzzer_passive = PWM(Pin(12))

# INIT
blue_led.freq(10000)
red_led.freq(10000)
blue_led.duty_u16(0)
red_led.duty_u16(0)

buzzer_active_as_passive.freq(1000)
buzzer_active_as_passive.duty_u16(0)

buzzer_passive.freq(1000)
buzzer_passive.duty_u16(0)


sensor_tick_alarm = Timer()

def send_sensors_values(timer):
    global luminosity
    luminosity_raw_value = luminosity.read_u16()
    message_manager.send_sensor_msg("P", str(luminosity_raw_value), "raw")
    
    raw_value, voltage, temperature_as_celsisus = get_thermometer_values(thermometer)
    message_manager.send_sensor_msg("T", str(raw_value), "raw")
    message_manager.send_sensor_msg("T", str(voltage), "voltage")
    message_manager.send_sensor_msg("T", str(temperature_as_celsisus), "celsius")
    
    if temperature_as_celsisus > 30:
        message_manager.send_raw_msg("ALERT !")
        buzzer_active_as_passive.freq(100)
        buzzer_active_as_passive.duty_u16(1000)
    else:
        buzzer_active_as_passive.duty_u16(0)

def message_actions_dispatcher(m: Message):
    if m.category == Message.M_CTG_COMMAND:
        if m.data == "Play a song !":
            music_player.playsong(default_song)
        else:
            message_manager.send_raw_msg("received command msg: " + m.data)
    else:
        message_manager.send_raw_msg("received msg category: " + m.category)
# RUN
try:
    message_manager.send_raw_msg("PICO started at " + str(time.time()))
    music_player = MusicPlayer(buzzer_passive, 1000)
    sensor_tick_alarm.init(mode=Timer.PERIODIC, period=10000, callback=send_sensors_values)

    while True:
        try:
            m = message_manager.read_msg()
            message_actions_dispatcher(m)
        except Exception as e:
            message_manager.send_raw_msg("exception: " + str(e))
            pass
        
        time.sleep(2)
        
except Exception as e:
    print(e)
    sensor_tick_alarm.deinit()
    blue_led.deinit()
    red_led.deinit()
    buzzer_active_as_passive.deinit()
    buzzer_passive.deinit()
