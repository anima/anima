from machine import PWM
from music.tones import SILENCE_TONE, TONE_FREQUENCY_DICT
from time import sleep

class MusicPlayer:
    # Duration while tone is being played (including silences)
    TONE_DURATION_S = 0.2

    buzzer: PWM
    sound_level: int

    def __init__(self, buzzer: PWM, sound_level: int):
        self.buzzer = buzzer
        self.sound_level = sound_level

    def _bequiet(self):
        self.buzzer.duty_u16(0)

    def _playtone(self, frequency: int):
        self.buzzer.duty_u16(self.sound_level)
        self.buzzer.freq(frequency)

    def playsong(self, song):
        for i in range(len(song)):
            tone_id = song[i][0]
            if (tone_id == SILENCE_TONE):
                self._bequiet()
            else:
                self._playtone(TONE_FREQUENCY_DICT[tone_id])
            sleep(MusicPlayer.TONE_DURATION_S * song[i][1])
        
        self._bequiet()
        