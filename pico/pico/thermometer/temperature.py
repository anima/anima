import math

def get_thermometer_values(adc):
    raw_value = adc.read_u16()
    voltage = raw_value / 65535.0 * 3.3
    thermistor_resistance = 10 * voltage / (3.3 - voltage)
    temperature_as_kelvin =  (1 / (1 / (273.15 + 25) + (math.log(thermistor_resistance/10)) / 3950))
    temperature_as_celsisus = int(temperature_as_kelvin - 273.15)
    
    return raw_value, voltage, temperature_as_celsisus