from shared.communication.message import Message
from os import path

DATA_FOLDER = "picohostagent/.data"
SENSORS_DATA_FILENAME = "sensors.json"


def message_actions_dispatcher(message: Message):
    if message.category == Message.M_CTG_SENSOR:
        _manage_sensor_message(message)
    else:
        print(str(message.timestamp) + " [" + message.category + "] : " + message.data)


def _manage_sensor_message(message: Message):
    with open(path.join(DATA_FOLDER, SENSORS_DATA_FILENAME), "a") as sensors_data_file:
        sensors_data_file.write(message.to_json() + ",")