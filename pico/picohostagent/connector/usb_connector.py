from serial import SerialException, Serial
from time import sleep

CONNECT_RECHECK_PERIOD_S = 3

def connect() -> Serial:
    try:
        print("Attempting to connect to PICO ...")
        while True:
            try:
                port = Serial(port="COM4", baudrate=9600, timeout=3, write_timeout=1)
                print("Connected to PICO !")
                return port
            except SerialException:
                pass
            sleep(CONNECT_RECHECK_PERIOD_S)
    except SerialException as conn_exception:
        print(conn_exception)
        print("[ERROR] Invalid device:", "COM4")
        exit(1)
