
from serial import SerialException
from time import sleep
import sys
from connector.usb_connector import connect
from shared.communication.message_manager import MessageManager
from message_interpreter import message_actions_dispatcher

MSG_READ_RECHECK_PERIOD_S = 5

def run():
    port = connect()
    message_manager = MessageManager(port, "pico-host-agent")

    while True:
        try:
            # READ ALL INCOMING MESSAGES
            while port.in_waiting > 0:
                m = message_manager.read_msg()
                message_actions_dispatcher(m) 
            
            # SEND ALL COMMANDS
            message_manager.send_cmd_msg("Play a song !")
            message_manager.send_cmd_msg("RANDOM")


            # WAIT BEFORE NEXT LOOP
            sleep(MSG_READ_RECHECK_PERIOD_S)
        except SerialException:
            # Keep listening for later reconnection
            port = connect()
            message_manager = MessageManager(port)

def main() -> int:
    try:
        run()
    except KeyboardInterrupt:
        print("PICO Host Agent shutdown.")
        return 0
    except Exception as e:
        print(e)
        return -1

if __name__ == '__main__':
    sys.exit(main())



