# PICO

Raspberry Pi Pico allows to build your own electronic card with some sensors + internal programmatic either in C or MicroPython.

In this project, MicroPython is being used to ease the interfacing between both programs + FastAPI.

## Main goals

`Pico` will be used as a physical station to :
- get data from its sensors (thermistor, photoresistor, ...) and 
  - behave accordingly ;
  - send it to a central service.
- provide some information to the user with its buzzer, LED, ...
- react to some commands

## Code organisation

```txt
|_ pico/            # code specific to Rapberry Pi Pico
|_ picohostagent/   # code specific to the relay Host Agent
|_ shared/          # contains all implementations to copy/paste as-is in the pico AND picohostagent folders.
```

## Architecture

```mermaid
flowchart TB
    subgraph "Local"
        PI[Pico]
        subgraph "Host"
            HA[Host-Agent]
            HAAPI[HTTP API]

            HAAPI x-..-x HA
        end
    end
    subgraph "Remote"
        API[Anima API]
    end

    PI --Transmit info like sensors data--> HA
    HA --Send commands--> PI
    HA -.Relay data.-> API
    API -.Send commands to Pico.->HAAPI

    subgraph "Legend"
        direction LR
        A --USB Transmission--> B
        C -.HTTP Transmission.-> D
        E x-.Internal Transmission.-x F
    end
```

Quick explanations :
- Raspberry Pi Pico (named `Pico` uin diagram), unlike Raspberry Pi Pico W, cannot send data over network. It can send data over USB though.
- `Host` is behaving as a relay :
  - It connects to `USB` port and: 
    - listen data coming from `Pico` ;
    - send commands to `Pico`.
  - It has a `HTTP` interface which can receive instructions (most likely from main `Anima API`) ;
  - It can send information to `Anima API` with `HTTP` requests.

## Communication steps

Communication is "standardized" using a common Message Manager and Message formatting which is easy to read/write.

High level steps are :

1. Initialize a `MessageManager` on `Pico` and `Host` ;
   - `Pico` will use `sys.stdin.buffer` as the "usb port" ;
   - `Host` will use `pyserial.Serial` connected to "COM4" (on Windows) as the "usb port".
2. Implement a reading loop / timer / ... ;
   - Call `MessageManager.read_msg()` which will read line by line data coming from `USB` ;
   - Process the message (you can use its `category` and `emitter` to chose the behavior).
3. Send data using `MessageManager.send_xxx_msg()` (check the most appropriate method according to your needs).

A later `ACK` may be implemented.
